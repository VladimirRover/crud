package main.java.com.VladimirRover.developerapp.controller;


import main.java.com.VladimirRover.developerapp.dao.DeveloperDAO;
import main.java.com.VladimirRover.developerapp.dao.JavaIODeveloperDAOImpl;
import main.java.com.VladimirRover.developerapp.model.Developer;

import java.io.IOException;
import java.util.List;

/**
 * Created by User on 22.05.2017.
 */
public class DeveloperController {

    private DeveloperDAO developerDAO = new JavaIODeveloperDAOImpl();


    public void addDeveloper(Developer developer) {
        this.developerDAO.addDeveloper(developer);
    }

    public void updateDeveloper(int id) throws IOException {
        this.developerDAO.updateDeveloper(id);
    }

    public void removeDeveloper(int id) throws IOException {
        this.developerDAO.removeDeveloper(id);
    }

    public String getDeveloperById(int id) {
        return this.developerDAO.getDeveloperById(id);
    }

    public List<String> listDevelopers() {
        return this.developerDAO.listDevelopers();
    }

}
