package main.java.com.VladimirRover.developerapp.model;

/**
 * Created by User on 22.05.2017.
 */
public class Developer {
    private int id;
    private String firstName;
    private String lastName;
    private String speciality;
    private int experience;
    private int salary;

    public Developer(){}

//    public Developer(int id, String firstName, String lastName, String speciality, int experience, int salary) {
//        this.id = id;
//        this.firstName = firstName;
//        this.lastName = lastName;
//        this.speciality = speciality;
//        this.experience = experience;
//        this.salary = salary;
//    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public int getExperience() {
        return experience;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "Developer{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", speciality='" + speciality + '\'' +
                ", experience=" + experience +
                ", salary=" + salary +
                '}';
    }
}
