package main.java.com.VladimirRover.developerapp.view;

import main.java.com.VladimirRover.developerapp.controller.DeveloperController;

import java.io.IOException;
import java.util.Scanner;

/**
 * Created by User on 22.05.2017.
 */
public class ConsoleHelper {

    private DeveloperController developerController = new DeveloperController();

    /**
     * Just shows graphic menu
     */
    private void menu() {

        System.out.println("=========================================");
        System.out.println("|              CRUD JAVA                |");
        System.out.println("=========================================");
        System.out.println("| Options:                              |");
        System.out.println("|        1. Create developer            |");
        System.out.println("|        2. Read developer              |");
        System.out.println("|        3. Update developer            |");
        System.out.println("|        4. Delete developer            |");
        System.out.println("|        5. Show all developers         |");
        System.out.println("|        6. Exit                        |");
        System.out.println("=========================================");

        System.out.println();
        System.out.print("Select option: ");
    }



    public static void updateMenu(){
        System.out.println("=========================================");
        System.out.println("| Options:                              |");
        System.out.println("|        1. Update first name           |");
        System.out.println("|        2. Update last name            |");
        System.out.println("|        3. Update speciality           |");
        System.out.println("|        4. Update experience           |");
        System.out.println("|        5. Update salary               |");
        System.out.println("|        6. Exit                        |");
        System.out.println("=========================================");
    }


    /**
     * Make your choice in graphic menu
     */
    public void makeChoice(){

        menu();

        Scanner input = new Scanner(System.in);

        String inputChoice;


        while ((inputChoice = input.next()) != null){

            if (inputChoice.equals("1")){

                DeveloperView developerView = new DeveloperView();
                developerController.addDeveloper(developerView.createDeveloper());

                menu();
            }


            if (inputChoice.equals("2")){

                DeveloperView developerView = new DeveloperView();

                System.out.println(developerController.getDeveloperById(developerView.readDeveloper()));

                System.out.println("--------------------------------------------------------------------------");
                System.out.println();

                menu();
            }


            if(inputChoice.equals("3")){
                DeveloperView developerView = new DeveloperView();
                try {
                    developerController.updateDeveloper(developerView.updateDeveloper());
                }
                catch (IOException e){
                    e.printStackTrace();
                }

                menu();

            }


            if (inputChoice.equals("4")){

                DeveloperView developerView = new DeveloperView();

                try {
                    developerController.removeDeveloper(developerView.deleteDeveloper());
                }
                catch (IOException e){
                    e.printStackTrace();
                }

                menu();
            }


            if (inputChoice.equals("5")){

                if (developerController.listDevelopers().size() > 0) {
                    for (int i = 0; i < developerController.listDevelopers().size(); i++) {

                        System.out.println(developerController.listDevelopers().get(i));
                    }
                }
                else {
                    System.out.println("The list is empty.");
                }

                menu();
            }


            if(inputChoice.equals("6")){
                System.out.println("Bye");
                System.exit(0);
            }
        }

    }
}
