package main.java.com.VladimirRover.developerapp.view;

import main.java.com.VladimirRover.developerapp.model.Developer;
import java.util.Scanner;

/**
 * Created by User on 28.05.2017.
 */
class DeveloperView {

   private static int id = 1;

   Developer createDeveloper(){

      Developer developer = new Developer();

      Scanner scanner = new Scanner(System.in);

      //Automatically developers id generator
      developer.setId(id);
      id++;

      System.out.println("Input first name");
      String firstName = scanner.next();
      developer.setFirstName(firstName);

      System.out.println("Input last name");
      String lastName = scanner.next();
      developer.setLastName(lastName);

      System.out.println("speciality");
      String speciality = scanner.next();
      developer.setSpeciality(speciality);

      System.out.println("developers experience");
      int experience = Integer.parseInt(scanner.next());
      developer.setExperience(experience);

      System.out.println("salary");
      int salary = Integer.parseInt(scanner.next());
      developer.setSalary(salary);


      return developer;
   }

   int updateDeveloper(){
      System.out.print("Select developer id to update: ");

      Scanner scanner = new Scanner(System.in);

      int dveloperIdToUpdate = Integer.parseInt(scanner.next());


      return dveloperIdToUpdate;
   }

   int readDeveloper(){

      System.out.print("Input developer id for search: ");

      Scanner scanner = new Scanner(System.in);

      int developersId = Integer.parseInt(scanner.next());


      return developersId;
   }

   int deleteDeveloper(){
      System.out.print("Select developers id to delete: ");

      Scanner scanner = new Scanner(System.in);

      int developersIdToDelete = Integer.parseInt(scanner.next());


      return developersIdToDelete;
   }
}
