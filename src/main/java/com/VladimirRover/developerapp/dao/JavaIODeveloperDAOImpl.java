package main.java.com.VladimirRover.developerapp.dao;

import main.java.com.VladimirRover.developerapp.model.Developer;
import main.java.com.VladimirRover.developerapp.view.ConsoleHelper;


import java.io.*;
import java.nio.file.Files;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

/**
 * Created by User on 22.05.2017.
 */
public class JavaIODeveloperDAOImpl implements DeveloperDAO {


    /**
     *This method interprets Developer object into
     *string view
     */
    private String developerToString(Developer developer){

        int id = developer.getId();
        String firstName = developer.getFirstName();
        String lastName = developer.getLastName();
        String speciality = developer.getSpeciality();
        int experience = developer.getExperience();
        int salary = developer.getSalary();

        String record = "id: "+ id +
               " first name: " + firstName +
                " last name: " +lastName +
                " speciality: " + speciality +
                " experience: " +experience +
                " salary: " + salary + "\n";
        return record;
    }


    /**
     * Source file filepath
     */
    private String fileWay = "src\\main\\resources\\IO.txt";


    /**
     * Source file
     */
    private File file = new File(fileWay);


    /**
     *This method adds Developer object in string view
     * in file IO.txt line by line
     */
    @Override
    public void addDeveloper(Developer developer) {

        try{
            FileWriter fileWriter = new FileWriter(file, true);
            fileWriter.write(developerToString(developer));
            fileWriter.flush();
            fileWriter.close();
        }
        catch (IOException e){
            e.printStackTrace();
        }

    }


    /**
     *This method update Developer
     *You can separate choose information to update
     */
    @Override
    public void updateDeveloper(int id) throws IOException {

        String developerToUpdate = getDeveloperById(id);

        System.out.println("Selected developer to update: " + developerToUpdate);

        /**
         * Temp file filepath
         */
        String tempFileWay = "src\\main\\resources\\temp.txt";

        /**
         * Create temp file to overwrite after
         * developer delete operation
         */
        File tempFile = new File(tempFileWay);

        try(BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(tempFile));) {

            String developersFromFile;

            while ((developersFromFile = bufferedReader.readLine()) != null){

                String trimmedLine = developersFromFile.trim();
                if (trimmedLine.equals(developerToUpdate)){

                    ConsoleHelper.updateMenu();

                    String[] splittedLine = trimmedLine.split(" ");


                    Scanner input = new Scanner(System.in);
                    String inputChoice;

                    while ((inputChoice = input.nextLine() ) != null){

                        String s = "";

                        Scanner scanner = new Scanner(System.in);

                        //first name
                        if(inputChoice.equals("1")){

                            System.out.print("Input new first name: ");

                            s = scanner.next();

                            System.out.println("First name: " + splittedLine[4] +" changed to: " + s);

                            splittedLine[4] = s;

                            ConsoleHelper.updateMenu();
                        }

                        //last name
                        if(inputChoice.equals("2")){

                            System.out.print("Input new last name: ");

                            s = scanner.next();

                            System.out.println("Last name: " + splittedLine[7] +" changed to: " + s);

                            splittedLine[7] = s;

                            ConsoleHelper.updateMenu();
                        }

                        //speciality
                        if(inputChoice.equals("3")){

                            System.out.print("Input new speciality: ");

                            s = scanner.next();

                            System.out.println("Speciality: " + splittedLine[9] +" changed to: " + s);

                            splittedLine[9] = s;

                            ConsoleHelper.updateMenu();
                        }

                        //experience
                        if(inputChoice.equals("4")){

                            System.out.print("Input new experience: ");

                            s = scanner.next();

                            System.out.println("Experience: " + splittedLine[11] +" changed to: " + s);

                            splittedLine[11] = s;

                            ConsoleHelper.updateMenu();
                        }

                        //salary
                        if(inputChoice.equals("5")){

                            System.out.print("Input new salary: ");

                            s = scanner.next();

                            System.out.println("Salary: " + splittedLine[13] +" changed to: " + s);

                            splittedLine[13] = s;

                            ConsoleHelper.updateMenu();
                        }

                        //exit
                        if(inputChoice.equals("6")){

                            System.out.println("Leaving update menu");
                            System.out.println();
                            break;
                        }

                    }


                    StringBuilder stringBuilder = new StringBuilder();

                    for (String s : splittedLine){
                        stringBuilder.append(s);
                        stringBuilder.append(" ");
                    }

                    bufferedWriter.write(stringBuilder.toString().trim() + System.getProperty("line.separator"));
                }
                else {
                    bufferedWriter.write(developersFromFile  + System.getProperty("line.separator"));
                }


            }
            bufferedWriter.flush();
            bufferedWriter.close();
            bufferedReader.close();

        }
        catch (IOException e){
            e.printStackTrace();
        }

        finally {
            Path p1 = Paths.get(fileWay);
            Path p2= Paths.get(tempFileWay);
            Files.move( p2, p1, REPLACE_EXISTING);
        }

    }


    /**
     * This method deleted developer selected by id
     * from IO.txt file
     * @param id
     * @throws IOException
     */
    @Override
    public void removeDeveloper(int id) throws IOException{

        String developerToDelete = getDeveloperById(id);

        System.out.println("Selected developer deleted: " + developerToDelete);

        /**
         * Temp file filepath
         */
        String tempFileWay = "src\\main\\resources\\temp.txt";

        /**
         * Create temp file to overwrite after
         * developer delete operation
         */
        File tempFile = new File(tempFileWay);

        try(BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(tempFile));) {

            String developersFromFile;

            while ((developersFromFile = bufferedReader.readLine()) != null){

                String trimmedLine = developersFromFile.trim();
                if (trimmedLine.equals(developerToDelete)){
                    continue;
                }
                bufferedWriter.write(developersFromFile  + System.getProperty("line.separator"));

            }
            bufferedWriter.flush();
            bufferedWriter.close();
            bufferedReader.close();

        }
        catch (IOException e){
            e.printStackTrace();
        }

        finally {
            Path p1 = Paths.get(fileWay);
            Path p2= Paths.get(tempFileWay);
            Files.move( p2, p1, REPLACE_EXISTING);
        }
    }


    /**
     * This method return and represent Developer in string format by id search
     */
    @Override
    public String getDeveloperById(int id) {

        String s = null;

        List<String> developersList = new ArrayList<>();


        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file))){

            String developersFromFile;

            while ((developersFromFile = bufferedReader.readLine()) != null){
                developersList.add(developersFromFile);
            }

            bufferedReader.close();
        }
        catch (IOException e){
            e.printStackTrace();
        }

        String[] idInLine ;
        for (int i = 0; i < developersList.size() ; i++) {


            idInLine = developersList.get(i).split(" ");

            int index = Integer.parseInt(idInLine[1]);

            if (index == id){
                s = developersList.get(i);
                break;
            }
            else{
                s = "There is no such id";
            }
        }

        return s ;

    }


    /**
     * This method returns exists Developers
     * from IO.txt file and add its into list.
     */
    @Override
    public List<String> listDevelopers()
    {
        List<String> existDevelopersList = new ArrayList<>();

        try{
            Scanner inputFromFile = new Scanner(file);
            while (inputFromFile.hasNext()){
                String line = inputFromFile.nextLine();
                existDevelopersList.add(line);
            }

            inputFromFile.close();
        }
        catch (IOException e){
            e.printStackTrace();
        }

        return existDevelopersList;
    }

}
