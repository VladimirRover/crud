package main.java.com.VladimirRover.developerapp.dao ;

import main.java.com.VladimirRover.developerapp.model.Developer;

import java.io.IOException;
import java.util.List;

/**
 * Created by User on 22.05.2017.
 */
public interface DeveloperDAO {

    void addDeveloper(Developer developer);

    void updateDeveloper(int id) throws IOException;

    void removeDeveloper(int id) throws IOException;

    String getDeveloperById(int id);

    List<String> listDevelopers();

}
